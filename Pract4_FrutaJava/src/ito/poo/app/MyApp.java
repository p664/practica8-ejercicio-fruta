package ito.poo.app;
import Pract4_FrutaUML.Fruta;
import Pract4_FrutaUML.Periodo;

public class MyApp {

	public static void main(String[] args) {
		
		Fruta f1=new Fruta("Manzana", 230.82f, 204.2f, 423.21f);
		Fruta f2=new Fruta("Durazno", 92.30f, 537.93f, 1930.60f);
	    System.out.println(f1);
	    System.out.println(f2);
	    
	    System.out.println(!f1.equals(f2));
	    System.out.println(f2.compareTo(f1));

	    Periodo p1=new Periodo("3 meses", 240.32f);
	    Periodo p2=new Periodo("5 meses", 530.00f);
	    System.out.println(p1);
	    System.out.println(p2);
	    
	    System.out.println(!p1.equals(p2));
	    System.out.println(p2.compareTo(p1));
	}
}